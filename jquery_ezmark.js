// $Id: hint.js,v 1.4 2009/12/20 02:05:12 quicksketch Exp $
(function ($) {

  /**
   * The Drupal behavior to add the $.hint() behavior to elements.
   */
  Drupal.behaviors.ezmark = {
    attach: function (context) {
      $('.ezmark input').ezMark();
    }
  };

})(jQuery);
